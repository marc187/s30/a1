const express = require(`express`);
const mongoose = require('mongoose');
const dotenv = require(`dotenv`);
const app = express();
const PORT = 3023;

dotenv.config();
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once(`open`, () => console.log(`Connected to Database`));



app.listen(PORT, console.log(`Listening to port ${PORT}`));

//middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))


const userSchema = new mongoose.Schema(
    {
        userName: {
            type: String,
            required: [true, `Username is required.`]
        },
        password: {
            type: String,
            required: [true, `Password is required.`]
        }
        
    }
)

const User = mongoose.model(`User`, userSchema)

app.post("/signup", (req, res) => {
    User.findOne({userName: req.body.userName}).then((result, err) => {
        if(result != null && result.userName == req.body.userName){
            return res.send(`Username ${req.body.userName} already exist.`)
        } else {
            let newUser = new User({
                userName: req.body.userName,
                password: req.body.password
            })
            newUser.save().then((savedUser, err) => {
                if(savedUser){
                    return res.send(`New user registered.`)
                } else {
                    return res.status(500).json(err)
                }
            })
        }
    })
})
